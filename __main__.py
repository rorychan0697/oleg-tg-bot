from flask import Flask, request
import telebot
from flask_cors import CORS, cross_origin
import json


TOKEN = "1784040890:AAESW-Vm8Fx8S7ONplTjDOjC5qtCqz_EL-8"

app = Flask(__name__)
CORS(app)
tb = telebot.TeleBot(TOKEN)
# tb.send_message(chatid, message)
chatId = ""
messages = {
	"1":"Подсказка ждёт тебя в маленькой чёрной сумочке.",
	"2":"Подсказка находится под пассажирским сидением.",
	"3":"Твоя подсказка там, где прячут что-то круглое и большое. Говорят, что без этого ещё и ехать нельзя...",
	"4":"Проверишь бардачок? Там всё на месте?",
	"5":"Да...... коврики в машине уже не те....",
	"6":"Под водительским сидением столько барахла!",
}


@app.route('/', methods=["POST"])
def index():
    global chatId, messages
    tb.send_message(chatId, messages[request.get_json()["msg"]])
    return json.dumps({"msg": "ok"})


@app.route('/sendid', methods=["POST"])
@cross_origin()
def sendId():
    global chatId
    if request.get_json()["chatId"] != None:
        chatId=request.get_json()["chatId"]
        print(chatId)
        return json.dumps({"msg": "ok"})
    else:
        return json.dumps({"msg": "error"})


@app.route('/isidexist', methods=["GET"])
def isIdExsist():
    global chatId
    if chatId == "":
        return json.dumps({"msg": "error"}), 200
    else:
        return json.dumps({"msg": "ok"}), 200

@app.route('/chatid/release', methods=["GET"])
def releaseChatId():
    global chatId
    chatId = ""
    return json.dumps({"msg": "ok"}), 200


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
