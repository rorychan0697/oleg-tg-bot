FROM python:latest
COPY ./__main__.py /app/__main__.py
COPY ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt
ENTRYPOINT ["python","__main__.py"]
